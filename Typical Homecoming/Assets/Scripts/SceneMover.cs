﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMover : MonoBehaviour {
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void Options()
    {
        SceneManager.LoadScene("Options");
    }
    public void GameOne()
    {
        SceneManager.LoadScene("SportLevel");
    }
    public void GameTwo()
    {
        SceneManager.LoadScene("DanceLevel");
    }
    public void GameThree()
    {
        SceneManager.LoadScene("PartyLevel");
    }
    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
    public void GameVictory()
    {
        SceneManager.LoadScene("Victory");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
