﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCharacter : MonoBehaviour 
{
	public float moveSpeed;
	private Transform dood;
	[HideInInspector]
	public float rotateSpeed;
	[HideInInspector]
	public float AImoveSpeed;
	
	// Use this for initialization
	void Start () 
	{
		dood = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input. GetKey(KeyCode.W))
		{
			dood.position = dood.position + (Vector3.up * (moveSpeed * Time.deltaTime)); 
		}
		if (Input. GetKey(KeyCode.S))
		{
			dood.position = dood.position + (-1 * (Vector3.up * (moveSpeed * Time.deltaTime))); 
		}
		if (Input. GetKey(KeyCode.A))
		{
			dood.position = dood.position + (-1 * (Vector3.right * (moveSpeed * Time.deltaTime))); 
		}
		if (Input. GetKey(KeyCode.D))
		{
			dood.position = dood.position + (Vector3.right * (moveSpeed * Time.deltaTime)); 
		}
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.gameObject.tag == "Enemy")
		{
			SportGameManager.instance.Tackled();
		}
	}
	
}
