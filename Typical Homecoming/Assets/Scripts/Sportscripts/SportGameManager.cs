﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SportGameManager : MonoBehaviour 
{
	public static SportGameManager instance = null;   //static instance of GameManager which allows it to be accessed by any other script
	public float score = 0;	//current score of players team
	public float enemyScore = 0; // current score of enemy team
	public float down = 1; 
	public Vector3 firstDownPosition;
	public Vector3 currentDownPosition;
	public float gameTimerMax;
	public float gameTimerCurrent;
	public bool paused;
	public GameObject winCanvas;
	public GameObject loseCanvas;
	public GameObject game;
	
	// Character Variables and list
	public List<GameObject> players;
	public GameObject gabe;
	public GameObject dann;
	public GameObject nico;
	public GameObject noah;
	public GameObject Izzy;
	public GameObject ross;
	public GameObject wade;
	public GameObject teve;
	public GameObject jeff;
	public GameObject dood;

	// Use this for initialization
	void Awake ()
	{
		if(instance == null)							//check if instance already exists
		
		instance = this;								//if not,set instance to this
		
		else if(instance != this)                      //if instance already exists and its not this:

		Destroy(gameObject);							//Sets this to not be destroyed when reloading scene
	}
	
	void Start()
	{
		players.Add(gabe);
		players.Add(dann);
		players.Add(nico);
		players.Add(noah);
		players.Add(Izzy);
		players.Add(ross);
		players.Add(wade);
		players.Add(teve);
		players.Add(jeff);
		players.Add(dood);
		firstDownPosition = dood.transform.position;
		gameTimerCurrent = gameTimerMax;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!paused)
		{
			gameTimerCurrent -= (1 * Time.deltaTime);
		}
		
		if (Input.GetKeyDown(KeyCode.K))
		{
			foreach(GameObject i in players)
			{
				i.SetActive(true);
				AI a = i.GetComponent<AI>();
				if(a != null)
				{
					a.AImoveSpeed = Random.Range(13, 17);
					a.rotateSpeed = Random.Range(6, 8);
				}
				
			}
			paused = false;
		}
		
		if (down >= 5)
		{
			dood.transform.position = firstDownPosition;
			EnemyReset();
			enemyScore += 7;
			down = 1;
		}
		
		if(gameTimerCurrent <= 0)
		{
			if (paused)
			{
				if(score > enemyScore)
				{
					foreach(GameObject i in players)
					{
						i.SetActive(false);
					}
					Victory();
				}
				if(enemyScore > score)
				{
					foreach(GameObject i in players)
					{
						i.SetActive(false);
					}
					Loss();
				}
			}
		}
	}
	
	public void Tackled()
	{
		foreach(GameObject i in players)
		{
			i.SetActive(false);
		}
		currentDownPosition = dood.transform.position;
		dood.transform.position = new Vector3 (currentDownPosition.x, currentDownPosition.y - currentDownPosition.y, currentDownPosition.z);
		EnemyReset();
		down += 1;
		paused = true;
	}
	
	public void Scored()
	{
		score += 7;
		foreach(GameObject i in players)
		{
			i.SetActive(false);
		}
		dood.transform.position = firstDownPosition;
		EnemyReset();
		down = 1;
		paused = true;
	}
	
	void EnemyReset()
	{
		gabe.transform.position = dood.transform.position + new Vector3(5.5f, 9.6f, 0f);
		dann.transform.position = dood.transform.position + new Vector3(5.5f, 7.18f, 0f);
		nico.transform.position = dood.transform.position + new Vector3(5.5f, 4.62f, 0f);
		noah.transform.position = dood.transform.position + new Vector3(5.5f, 2.3f, 0f);
		Izzy.transform.position = dood.transform.position + new Vector3(5.5f, -0.7f, 0f);
		ross.transform.position = dood.transform.position + new Vector3(5.5f, -3.23f, 0f);
		wade.transform.position = dood.transform.position + new Vector3(5.5f, -5.3f, 0f);
		teve.transform.position = dood.transform.position + new Vector3(5.5f, -7.8f, 0f);
		jeff.transform.position = dood.transform.position + new Vector3(5.5f, -10f, 0f);
	}
	
	public void OutofBounds()
	{
		foreach(GameObject i in players)
		{
			i.SetActive(false);
		}
		currentDownPosition = dood.transform.position;
		dood.transform.position = new Vector3 (currentDownPosition.x, currentDownPosition.y - currentDownPosition.y, currentDownPosition.z);
		EnemyReset();
		paused = true;
		down += 1;
	}
	
	public void TouchBack()
	{
		foreach(GameObject i in players)
		{
			i.SetActive(false);
		}
		paused = true;
		dood.transform.position = firstDownPosition;
		EnemyReset();
		down += 1;
		enemyScore += 2;
		
	}
	
	void Victory()
	{
		winCanvas.SetActive(true);
		game.SetActive(false);
	}
	
	void Loss()
	{
		loseCanvas.SetActive(true);
		game.SetActive(false);
	    SceneManager.LoadScene("GameOver");
    }
}
