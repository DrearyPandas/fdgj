﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchbackScript : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			SportGameManager.instance.TouchBack();
		}
	}	
}
