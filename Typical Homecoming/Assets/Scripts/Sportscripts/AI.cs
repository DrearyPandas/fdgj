﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {
	private Transform aiguy;
	public float AImoveSpeed;
	public GameObject Player;
	public float rotateSpeed;
	public Rigidbody2D aiguyRB;
	
	
	
	// Use this for initialization
	void Start () 
	{
		aiguy = GetComponent<Transform>();
		aiguyRB = GetComponent<Rigidbody2D>();
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 localPosition = Player.transform.position - aiguy.position; // take difference of player location and self location
        localPosition = localPosition.normalized; // normalize vector
        float angle = Mathf.Atan2(localPosition.y, localPosition.x) * Mathf.Rad2Deg; // vector to degrees
        transform.rotation = Quaternion.Lerp((this.gameObject.transform.rotation), Quaternion.Euler(0f, 0f, angle - 90), (Time.deltaTime * rotateSpeed)); // lerping quaternions in order to scale rotation speed
		aiguy.position += transform.up * (Time.deltaTime * AImoveSpeed);
	}

}

