﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyFollowScript : MonoBehaviour 
{
	public GameObject mirrorPos;
	
	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.position = mirrorPos.transform.position;
	}
}
