﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RhythmGameManager : MonoBehaviour 
{
	public static RhythmGameManager instance;
	public GameObject player;
	public GameObject upArrow;
	public GameObject downArrow;
	public GameObject leftArrow;
	public GameObject rightArrow;
	public float beatTimeMax;
	public float beatTimecurrent;
	public float score;
	public ArrowScript arrowScript;
	public float gameTimerMax;
	public float gameTimerCurrent;
	public GameObject winScreen;
	public GameObject loseScreen;
	public float winScore;
	private float gameDelay = 10;
	

	void Awake()
	{
		if (instance == null) // if no instance
		{
			instance = this; // this is it
		}
	}
	
	// Use this for initialization
	void Start () 
	{
		gameTimerCurrent = gameTimerMax;
		beatTimecurrent = beatTimeMax;
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameDelay -= 1 * Time.deltaTime;
		if(gameDelay <=0)
		{
			gameTimerCurrent -= (1 * Time.deltaTime);
			beatTimecurrent -= (1 * Time.deltaTime);
			if (beatTimecurrent <= 0)
			{
				Beat();
				beatTimecurrent = beatTimeMax;
			}
			if (gameTimerCurrent <= 0)
			{
				if (score >= winScore)
				{
					Victory();
				}
				else
				{
					Lose();
				}
			}
		}
	}
	
	void Beat()
	{
		int pick = Random.Range(0,4);
		
		if (pick == 0)
		{
			upArrow.SetActive(true);
		}
		if (pick == 1)
		{
			downArrow.SetActive(true);
		}
		if (pick == 2)
		{
			leftArrow.SetActive(true);
		}
		if (pick == 3)
		{
			rightArrow.SetActive(true);
		}
	}
	
	void Victory()
	{
        gameObject.SetActive(false);
	    SceneManager.LoadScene("Victory");
    }
	
	void Lose()
	{
        gameObject.SetActive(false);
	    SceneManager.LoadScene("GameOver");
    }
}
