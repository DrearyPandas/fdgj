﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowScript : MonoBehaviour 
{
	public GameObject arrow; // arrow storage
	public float lifeTime; // current arrow existence time
	public float lifeTimeMax; // max time for arrow to exist

	// Use this for initialization
	void Start () 
	{
		lifeTime = lifeTimeMax; // sets the current lifetime to max
		arrow = this.gameObject; // sets arrow to this object
		RhythmGameManager.instance.arrowScript = this; // sets the arrowscript 
	}
	
	// Update is called once per frame
	void Update () 
	{
		lifeTime -= (1 * Time.deltaTime);
		if (lifeTime <= 0)
		{
			RhythmGameManager.instance.score -= 50;
			lifeTime = lifeTimeMax;
			arrow.SetActive(false);
		}
		
		if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
		{
			if (gameObject.name == "Wade Testarrow Left")
			{
				RhythmGameManager.instance.score += 100;
				lifeTime = lifeTimeMax;
				arrow.SetActive(false);
			}
			else
			{
				RhythmGameManager.instance.score -= 50;
			}
		}
		
		if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			if (gameObject.name == "Wade Testarrow Right")
			{
				RhythmGameManager.instance.score += 100;
				lifeTime = lifeTimeMax;
				arrow.SetActive(false);
			}
			else
			{
				RhythmGameManager.instance.score -= 50;
			}
		}
		
		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			if (gameObject.name == "Wade Testarrow Up")
			{
				RhythmGameManager.instance.score += 100;
				lifeTime = lifeTimeMax;
				arrow.SetActive(false);
			}
			else
			{
				RhythmGameManager.instance.score -= 50;
			}
		}
		
		if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			if (gameObject.name == "Wade Testarrow Down")
			{
				RhythmGameManager.instance.score += 100;
				lifeTime = lifeTimeMax;
				arrow.SetActive(false);
			}
			else
			{
				RhythmGameManager.instance.score -= 50;
			}
		}
	}
}
