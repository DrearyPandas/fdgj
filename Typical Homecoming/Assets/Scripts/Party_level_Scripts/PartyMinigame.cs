﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyMinigame : MonoBehaviour 
{
	public GameObject speech;
	public bool playGame;
	public GameObject game;
	public GameObject party;

	
	// Update is called once per frame
	void Update () 
	{
		if(playGame)
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				game.SetActive(true);
				party.SetActive(false);
			}
				
		}
	}

	
	public void Talk()
	{
		speech.SetActive(true);
		playGame = true;
	}
	
	public void StopTalk()
	{
		speech.SetActive(false);
	}
}
