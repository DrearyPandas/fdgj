﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChugMeter : MonoBehaviour
{

    public Image imageC; //variable that holds the image component
    public float timer;//timer

    // Use this for initialization
    void Start ()
    {
        imageC = GetComponent<Image>();
        imageC.fillAmount = 0;
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;// tracks time
        GameManager.instance.percentOfMaxChug = GameManager.instance.playerCurrentChugMeter / GameManager.instance.MAXCHUGMETER;
        imageC.fillAmount = GameManager.instance.percentOfMaxChug;

        if (GameManager.instance.playerCurrentChugMeter > 0)
        {
            if (timer > GameManager.instance.cMRateOfLoss)
            {
                timer = 0;
                GameManager.instance.playerCurrentChugMeter -= GameManager.instance.amountLost;
            }
             
        }
        else if (GameManager.instance.playerCurrentChugMeter == 100)
        {
            //you win send back to party scene
        }
        else
        {
            //do nothing
        }
    }
}