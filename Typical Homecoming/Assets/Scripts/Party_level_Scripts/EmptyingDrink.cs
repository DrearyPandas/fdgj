﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmptyingDrink : MonoBehaviour
{

    public Image imageComponent;
    public float timer;

	// Use this for initialization
	void Start ()
	{
	    imageComponent = GetComponent<Image>();
	    imageComponent.fillAmount = 1;
	    GameManager.instance.currentDrinkFillAmount = GameManager.instance.MAXFILLAMOUNT;


    }
	
	// Update is called once per frame
	void Update ()
	{
	    timer += Time.deltaTime;
	    float percentOfMaximum = GameManager.instance.currentDrinkFillAmount / GameManager.instance.MAXFILLAMOUNT;
	    imageComponent.fillAmount = percentOfMaximum;

        
	    if (GameManager.instance.currentDrinkFillAmount > 0)
	    {
	        if (timer > GameManager.instance.drainSpeed)
	        {
	            if (GameManager.instance.percentOfMaxChug < .25)
	            {
	                timer = 0;
	                GameManager.instance.currentDrinkFillAmount -= GameManager.instance.emptyAmount1;
	            }
	            else if (GameManager.instance.percentOfMaxChug > .25 && GameManager.instance.percentOfMaxChug < .50)
	            {
	                timer = 0;
                    GameManager.instance.currentDrinkFillAmount -= GameManager.instance.emptyAmount2;
	            }
	            else if (GameManager.instance.percentOfMaxChug > .50 && GameManager.instance.percentOfMaxChug < .75)
	            {
	                timer = 0;
                    GameManager.instance.currentDrinkFillAmount -= GameManager.instance.emptyAmount3;
	            }
	            else if (GameManager.instance.percentOfMaxChug > .75)
	            {
	                timer = 0;
                    GameManager.instance.currentDrinkFillAmount -= GameManager.instance.emptyAmount4;
	            }
	        }
	    }
	}
}
