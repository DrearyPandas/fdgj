﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_Chug_Meter : MonoBehaviour
{
    public float timer;
    public Image enemyImageC;

	// Use this for initialization
    void Start()
    {
        enemyImageC = GetComponent<Image>();
        enemyImageC.fillAmount = 0;
    }

    // Update is called once per frame
	void Update ()
	{
	    timer += Time.deltaTime;
	    GameManager.instance.percentOfMaxOpponentChug = GameManager.instance.oppenentCurrentChugMeter / GameManager.instance.MAXCHUGMETER;
	    enemyImageC.fillAmount = GameManager.instance.percentOfMaxOpponentChug;
	    int pickFillAmount = Random.Range(1, 3);
	    if (timer > GameManager.instance.opponentSpeed)
	    {
	        if (pickFillAmount == 1)
	        {
	            GameManager.instance.oppenentCurrentChugMeter += GameManager.instance.fillAmount1;
	        }
            else if (pickFillAmount == 2)
	        {
	            GameManager.instance.oppenentCurrentChugMeter += GameManager.instance.fillAmount2;
	        }
        }

	    if (GameManager.instance.oppenentCurrentChugMeter > 0)
	    {
	        if (timer > GameManager.instance.cMRateOfLoss)
	        {
	            timer = 0;
	            GameManager.instance.oppenentCurrentChugMeter -= GameManager.instance.amountLost;
	        }
	    }
	}
}
