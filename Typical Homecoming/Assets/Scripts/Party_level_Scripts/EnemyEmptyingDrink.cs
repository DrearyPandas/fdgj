﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyEmptyingDrink : MonoBehaviour
{

    public Image imageComponent;
    public float timer;

	// Use this for initialization
	void Start ()
	{
	    imageComponent = GetComponent<Image>();
	    imageComponent.fillAmount = 1;
	    GameManager.instance.currentOpponentDrinkfillAmount = GameManager.instance.MAXFILLAMOUNT;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    timer += Time.deltaTime;
	    float percentOfMaxfill = GameManager.instance.currentOpponentDrinkfillAmount / GameManager.instance.MAXFILLAMOUNT;
	    imageComponent.fillAmount = percentOfMaxfill;

	    if (GameManager.instance.currentOpponentDrinkfillAmount > 0)
	    {
	        if (timer > GameManager.instance.drainSpeed)
	        {
	            if (GameManager.instance.percentOfMaxOpponentChug < .25)
	            {
	                timer = 0;
	                GameManager.instance.currentOpponentDrinkfillAmount -= GameManager.instance.emptyAmount1;
	            }
	            else if (GameManager.instance.percentOfMaxOpponentChug > .25 && GameManager.instance.percentOfMaxOpponentChug < .50)
	            {
	                timer = 0;
	                GameManager.instance.currentOpponentDrinkfillAmount -= GameManager.instance.emptyAmount2;
	            }
	            else if (GameManager.instance.percentOfMaxOpponentChug > .50 && GameManager.instance.percentOfMaxOpponentChug < .75)
	            {
	                timer = 0;
	                GameManager.instance.currentOpponentDrinkfillAmount -= GameManager.instance.emptyAmount3;
	            }
	            else if (GameManager.instance.percentOfMaxOpponentChug > .75)
	            {
	                timer = 0;
	                GameManager.instance.currentOpponentDrinkfillAmount -= GameManager.instance.emptyAmount4;
	            }
            }
	    }
	}
}
