﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    //variable for this gameObjects transform component
    public Transform tf;

    public virtual void Start()
    {
        tf = GetComponent<Transform>();
    }

    public virtual void MoveForward()
    {

    }

    public virtual void MoveBack()
    {

    }

    public virtual void RotateLeft()
    {

    }

    public virtual void RotateRight()
    {

    }
}
