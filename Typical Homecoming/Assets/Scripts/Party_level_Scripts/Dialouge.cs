﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialouge : MonoBehaviour 
{

	public GameObject speech;
	
	public void Talk()
	{
		speech.SetActive(true);
	}
	
	public void StopTalk()
	{
		speech.SetActive(false);
	}
}
