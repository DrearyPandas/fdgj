﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    [Header("Player Settings")]
    public float playerMoveSpeed;
    public float playerRotateSpeed;
    public float playerChugAmount;
    public float percentOfMaxChug;
    public float currentDrinkFillAmount;

    [Header("Opponent Settings")]
    public float opponentChugMeter;
    public float oppenentChugAmount;
    public float oppenentCurrentChugMeter;
    public float fillAmount1;
    public float fillAmount2;
    public float percentOfMaxOpponentChug;
    public float currentOpponentDrinkfillAmount;

    [Header("Chug Settings")]
    public float MAXCHUGMETER;
    public float playerCurrentChugMeter;
	public float playerMaxHealth; // added to fix compiler error -wade
    public float cMRateOfLoss;// rate the meter is unfilled
    public float amountLost;
    public float opponentSpeed;
    public float MAXFILLAMOUNT;
    public float drainSpeed;
    public float emptyAmount1;
    public float emptyAmount2;
    public float emptyAmount3;
    public float emptyAmount4;





    // Use this for initialization
    void Awake () {
	    if (instance == null)
	    {
	        instance = this;
	    }
	    else
	    {
	        Destroy(gameObject);
	    }
	}
	
	// Update is called once per frame
	void Update ()
	{
    }
}
