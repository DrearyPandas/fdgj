﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnPlayer : Pawn {

	// Use this for initialization
	public override void Start ()
	{
	    base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
	    {
            MoveForward();
	    }

	    if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))

        {
            MoveBack();
	    }

	    if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))

        {
            RotateRight();
	    }

	    if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
	    {
            RotateLeft();
	    }

    }

    public override void MoveForward()
    {
        tf.position += tf.rotation * (Vector3.up * GameManager.instance.playerMoveSpeed * Time.deltaTime);
    }

    public override void MoveBack()
    {
        tf.position += tf.rotation * (Vector3.down * GameManager.instance.playerMoveSpeed * Time.deltaTime);
    }

    public override void RotateRight()
    {
        tf.Rotate(Vector3.back * GameManager.instance.playerRotateSpeed * Time.deltaTime);
    }

    public override void RotateLeft()
    {
        tf.Rotate(Vector3.forward * GameManager.instance.playerRotateSpeed * Time.deltaTime);
    }
}
