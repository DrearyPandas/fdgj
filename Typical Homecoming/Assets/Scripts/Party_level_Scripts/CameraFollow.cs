﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{

	public GameObject followObject;

	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.position = followObject.transform.position + new Vector3 (0, 0, -10);
	}
}
