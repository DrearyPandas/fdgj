﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour 
{
	public GameObject nearby;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.E))
		{
			if(!nearby.gameObject.GetComponent<Dialouge>())
			{
				nearby.gameObject.GetComponent<PartyMinigame>().Talk();
			}
			else
			{
				nearby.gameObject.GetComponent<Dialouge>().Talk();
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "NPC")
		{
			nearby = coll.gameObject;
		}
	}
	
	void OnTriggerExit2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "NPC")
		{
			if(!nearby.gameObject.GetComponent<Dialouge>())
			{
				nearby.gameObject.GetComponent<PartyMinigame>().StopTalk();
			}
			else
			{
				nearby.gameObject.GetComponent<Dialouge>().StopTalk();
			}
			nearby = null;
		}
	}
}
