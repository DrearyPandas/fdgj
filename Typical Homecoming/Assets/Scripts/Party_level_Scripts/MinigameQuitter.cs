﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameQuitter : MonoBehaviour 
{
	public GameObject game;
	public GameObject party;

	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			game.SetActive(false);
			party.SetActive(true);
		}
	}
}
