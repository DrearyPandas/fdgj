﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrobeColor : MonoBehaviour
{
    public SpriteRenderer strobeLight;
    public float colorOne;
    public float colorTwo;
    public float colorThree;
    public float colorTime;

	// Use this for initialization
	void Start ()
	{
	    strobeLight = GetComponent<SpriteRenderer>();
	    StartCoroutine(colorTimer());
	}
	
	// Update is called once per frame
	void Update ()
	{
        strobeLight.color = new Color(colorOne,colorTwo,colorThree);
    }

    IEnumerator colorChanger()
    {
        yield return new WaitForSeconds(0);
        colorOne = Random.Range(0f, 1f);
        colorTwo = Random.Range(0f, 1f);
        colorThree = Random.Range(0f, 1f);
        StartCoroutine(colorTimer());
    }

    IEnumerator colorTimer()
    {
        yield return new WaitForSeconds(colorTime);
        StartCoroutine(colorChanger());
    }
}
